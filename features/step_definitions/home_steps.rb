Dado("que eu acesso o site das Casas Bahia") do    
   @home.load
end

Quando("eu pesquiso por {string}") do |string|
   @home.preencher_busca(string)    
   #page.find('div.ac-product-title').should have_content("PRODUTOS SUGERIDOS")    
end

Quando("quando eu clico no botão buscar") do 
   page.driver.browser.manage.delete_all_cookies
   @home.buscar
end

Então("eu quero ser redirecionado para a página com o resultado da busca com {string}") do |string|   
   expect(page).to have_content string    
end

Quando("eu seleciono um item qualquer sou redirecionado para pagina que tem botão {string}") do |string|
   page.driver.browser.manage.delete_all_cookies
   @resultadoBusca.seleciona_primeiro_item
   expect(@selecaoItem.botao_comprar).to have_text(string)
end

Quando("eu clico no botão Comprar quero ir para página {string}") do |string|
   page.driver.browser.manage.delete_all_cookies
   @selecaoItem.comprar
   expect(page.current_url).to have_content string   
   
end

Então("eu quero selecionar um serviço e continuar") do   
   page.driver.browser.manage.delete_cookie 'ak_bmsc'
   @carrinhoServicos.selecionarServico
   @carrinhoServicos.continuar
end

Então("eu quero mudar a quantidade de produtos para {string}") do |string| 
   page.driver.browser.manage.delete_cookie 'ak_bmsc'
   @carrinhoConcluir.informar_quantidade string
end

Então("eu quero informar o cep {string}") do |string| 
   page.driver.browser.manage.delete_cookie 'ak_bmsc'
   @carrinhoConcluir.informar_cep string
end

Então("eu quero concluir") do   
   @carrinhoConcluir.finalizar
end