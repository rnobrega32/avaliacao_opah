require 'capybara'
require 'capybara/cucumber'
require 'capybara/rspec'
require 'selenium-webdriver'
require 'site_prism' 


Capybara.register_driver :site_prism do |app|
  chrome_options = Selenium::WebDriver::Chrome::Options.new
  chrome_options.add_argument('--user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0');

  profile = Selenium::WebDriver::Firefox::Profile.new
  profile['general.useragent.override'] = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:85.0) Gecko/20100101 Firefox/85.0'

  opts = Selenium::WebDriver::Firefox::Options.new(profile: profile)

  Capybara::Selenium::Driver.new(app, browser:  :chrome, options: chrome_options)
end

# Configurando o driver
Capybara.configure do |config|
  config.run_server = false
  Capybara.default_driver = :site_prism
  Capybara.page.driver.browser.manage.window.maximize 
  config.default_max_wait_time = 10 
  config.app_host = 'https://www.casasbahia.com.br'
  
end