#language: pt

Funcionalidade: Acessar a página as Casas Bahia

    COMO cliente das Casas Bahia EU QUERO acessar a página www.casasbahia.com.br A FIM de 
    buscar por produtos

    Cenário: Acessar a página das Casas Bahia
        Dado que eu acesso o site das Casas Bahia
        Quando eu pesquiso por "Apple iPhone 7"
        E quando eu clico no botão buscar
        Então eu quero ser redirecionado para a página com o resultado da busca com "Você buscou por apple iphone 7"
    Cenário: Selecionar um item comprar        
        Dado que eu acesso o site das Casas Bahia
        Quando eu pesquiso por "Apple iPhone 7"
        E quando eu clico no botão buscar
        Então eu quero ser redirecionado para a página com o resultado da busca com "Você buscou por apple iphone 7"
        Quando eu seleciono um item qualquer sou redirecionado para pagina que tem botão "Comprar"
        Quando eu clico no botão Comprar quero ir para página "https://carrinho.casasbahia.com.br"
        Então eu quero selecionar um serviço e continuar
        Então eu quero mudar a quantidade de produtos para "2"
        Então eu quero informar o cep "86400000"
        Então eu quero concluir

        

