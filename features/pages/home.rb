class Home < SitePrism::Page
    set_url '/'
    #set_url '/Apple-iPhone-7/b'
    element :campo_busca, '#strBusca'
    element :botao_buscar, '#btnOK'    
    

    def preencher_busca(texto)
        campo_busca.set texto
    end

    def buscar
        botao_buscar.click
    end

    def seleciona_primeiro_item
        primeiro_item_da_busca[0].click
    end

end