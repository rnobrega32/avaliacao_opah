class CarrinhoConcluir < SitePrism::Page
    
    element :botao_concluir, "a.bt.btConcluir"
    element :campo_quantidade, "fieldset.itemQty > input"
    element :campo_cep, "#Cep"
    element :botao_consultar_cep, "#btnCalcularFrete"

    def informar_quantidade(quantidade)
        campo_quantidade.set quantidade
    end

    def informar_cep(cep)        
        campo_cep.set cep
        #botao_consultar_cep.click
    end

    def finalizar
        botao_concluir.click
    end

end