class ResultadoBusca < SitePrism::Page
    elements :primeiro_item_da_busca, 'li.iWpyBK a'

    def seleciona_primeiro_item
        primeiro_item_da_busca[0].click
    end

end