class CarrinhoServicos < SitePrism::Page
    elements :servicos, "span.spanRadioCheck"
    element :botao_continuar, "button.decline-button"

    def temServicos
        return servicos.count > 0
    end

    def selecionarServico
        servicos[2].click
    end

    def continuar
        botao_continuar.click
    end

end