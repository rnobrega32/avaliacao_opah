class SelecaoItem < SitePrism::Page
    element :botao_comprar, '#buy-box > button'    
    element :campo_cep, '#frete'

    def preenche_cep(cep)
        campo_cep.set cep
    end

    def comprar
        botao_comprar.click
    end

end